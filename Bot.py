import botconfig

import sys, string, time

from threading import Timer

failed_tick = None
jobs_tick = None

# Keep our last object around for keepsies
last_job = None

# Redis-Queue imports
import redis
from rq import Queue, Worker, job, connections, get_failed_queue


class Bot:
    ####################################################################
    def __init__(self, irc, console):
        # Get our object references
        self.irc = irc
        self.console = console
        # Tell the IRC class who we are.
        self.irc.register_bot(self)
        # Tell the server who we are.
        self.irc.push("NICK {}\n".format(botconfig.NICK))
        self.irc.push("USER {} * * :{}\n".format(botconfig.NICK, botconfig.NICK))
        # Set up our redis feed
        self.myredis = redis.StrictRedis(host='localhost', port=6379, db=0, password=None)
        self.q = Queue(connection=self.myredis)
        self.failedq = get_failed_queue(connection=self.myredis)


    ####################################################################
    def failed_timer_tick(self):
        global failed_tick
        #do things
        self.poll_failed()
        failed_tick = Timer(5, self.failed_timer_tick)
        failed_tick.start()

    def jobs_timer_tick(self):
        global jobs_tick
        #do things
        self.poll_jobs()
        jobs_tick = Timer(0.1, self.jobs_timer_tick)
        jobs_tick.start()

    def list_workers(self):
        def serialize_queue_names(worker):
            return [q.name for q in worker.queues]

        workers = [dict(name=worker.name, queues=serialize_queue_names(worker),
            state=worker.state) for worker in Worker.all(connection=self.myredis)]
        return workers

    def serialize_jobs(self, jobs):
        return [dict(func_name=job.func_name, args=job.args, status=job.status, id=job.id) for job in jobs]

    def list_jobs(self):
        jobs = {}
        try:
            job_list = self.q.get_jobs()
        except redis.exceptions.ConnectionError:
            self.irc.push("PRIVMSG {} :Failed to connect to redis.\n".format(botconfig.CHANNEL))
            return []
        jobs = self.serialize_jobs(sorted(job_list))
        return jobs

    def list_failed(self):
        jobs = {}
        try:
            job_list = self.failedq.get_jobs()
        except redis.exceptions.ConnectionError:
            self.irc.push("PRIVMSG {} :Failed to connect to redis.\n".format(botconfig.CHANNEL))
            return []
        jobs = self.serialize_jobs(sorted(job_list))
        return jobs


    def poll_failed(self):
        failed = self.list_failed()
        if len(failed) > 0:
            self.irc.push("PRIVMSG {} :{}\n".format(botconfig.CHANNEL, failed))
        else:
            if failed_tick == None:
                self.irc.push("PRIVMSG {} :Empty reply\n".format(botconfig.CHANNEL))

    def poll_jobs(self):
        jobs = self.list_jobs()
        if len(jobs) > 0:
            self.irc.push("PRIVMSG {} :{}\n".format(botconfig.CHANNEL, jobs))
        else:
            if jobs_tick == None:
                self.irc.push("PRIVMSG {} :Empty reply\n".format(botconfig.CHANNEL))

    def play_dead(self, user, nick, channel):
        if user in botconfig.OWNERS:
            print('YES MASSA RIGHT AWAY MASSA')
            self.irc.push("PRIVMSG {} :YES MASSA RIGHT AWAY MASSA\n".format(channel))
            # Kill off our timers
            #jobs_tick.cancel()   # BROEKN
            #failed_tick.cancel()
            # Tell the server we're quitting.
            self.irc.push("QUIT :Yes {}, playing dead.\n".format(nick))
        else:
            print('{} tried to kill me...'.format(nick))
            self.irc.push(
                "PRIVMSG {} :HELP HELP I'M BEING REPRESSED, SEE THE VIOLENCE INHERENT IN THE SYSTEM\n".format(channel))


    ####################################################################

    def process(self, line, prefix, nick, user, host, command, params, rest):
        debug_babble = False
        debug_babble_grok = False

        # First determine if this is a numerical message id
        num_command = 0
        try:
            num_command = int(command)
        except ValueError: # It wasn't a number.
            if command == "NOTICE":
                num_command = 50
            if command == "PRIVMSG":
                num_command = 51

        # Is it an ignored or interesting message id?
        if num_command not in botconfig.IGNORE_MSGIDS:
            debug_babble = True
        if num_command in botconfig.INTERESTING_MSGIDS:
            debug_babble_grok = True

        # Other commands
        if rest == 'play dead':
            debug_babble = True
            debug_babble_grok = True
            self.play_dead(user, nick, params[0])

        # Polling for the failed queue
        if rest == 'poll failed start':
            print('{} triggered starting my poll failed endpoint.'.format(nick))
            debug_babble = True
            debug_babble_grok = True
            self.failed_timer_tick()

        if rest == 'poll failed stop':
            print('{} triggered stopping my poll failed endpoint.'.format(nick))
            debug_babble = True
            debug_babble_grok = True
            global failed_tick
            failed_tick.cancel()

        if rest == 'poll failed':
            print('{} triggered my poll failed endpoint.'.format(nick))
            debug_babble = True
            debug_babble_grok = True
            self.poll_failed()

            # Polling for the job queue
        if rest == 'poll jobs start':
            print('{} triggered starting my poll failed endpoint.'.format(nick))
            debug_babble = True
            debug_babble_grok = True
            self.jobs_timer_tick()

        if rest == 'poll jobs stop':
            print('{} triggered stopping my poll failed endpoint.'.format(nick))
            debug_babble = True
            debug_babble_grok = True
            global jobs_tick
            jobs_tick.cancel()

        if rest == 'poll jobs':
            print('{} triggered my poll jobs endpoint.'.format(nick))
            debug_babble = True
            debug_babble_grok = True
            self.poll_jobs()

        # Tells you about an object's method calls. Hides intrinsics. Use eval dir(obj) instead.
        if rest and rest[:4] == 'dir ':
            debug_babble_grok = True
            if user in botconfig.OWNERS:
                try:
                    result = [x for x in dir(eval(rest[4:])) if x[:1] != '_']
                except BaseException, e:
                    result = e
                self.irc.push("PRIVMSG {} :Result: {}\n".format(botconfig.CHANNEL, result))

        # Stashes the last object returned as 'last_job' so you can poke it with eval
        if rest and rest[:5] == 'test ':
            debug_babble_grok = True
            if user in botconfig.OWNERS:
                global last_job
                try:
                    last_job = eval(rest[5:])
                    time.sleep(5)
                    result = last_job.result
                except BaseException, e:
                    result = e
                self.irc.push("PRIVMSG {} :Result: {}\n".format(botconfig.CHANNEL, result))


        # Here's the original real magic eval function.
        if rest and rest[:5] == 'eval ':
            debug_babble_grok = True
            if user in botconfig.OWNERS:
                try:
                    result = eval(rest[5:])
                except BaseException, e:
                    result = e
                self.irc.push("PRIVMSG {} :Result: {}\n".format(botconfig.CHANNEL, result))


        # Print some debug crap
        if debug_babble:
            print("DEBUG: line: {}".format(line))
        if debug_babble_grok:
            print("DEBUG_GROK: prefix: {}, nick: {}, user: {}, host: {}".format(prefix, nick, user, host))
            print("DEBUG_GROK: command: {}, params: {}, rest: {}".format(command, params, rest))
