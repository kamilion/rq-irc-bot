import botconfig
import asyncore, asynchat, ssl, socket, traceback, sys, collections, string, times


class IRC(asynchat.async_chat):
    def __init__(self):
        addrinfo = socket.getaddrinfo(botconfig.HOST, botconfig.PORT, socket.AF_UNSPEC, socket.SOCK_STREAM, 0,
                                      socket.AI_ADDRCONFIG | socket.AI_V4MAPPED | socket.AI_ALL)
        for family, socktype, protocol, _, address in addrinfo:
            try:
                sock = socket.socket(family, socktype, protocol)
                sock.connect(address)
                break
            except Exception:
                continue
        else:
            sys.exit("Failed to connect to " + botconfig.HOST)

        # SSL Setup
        # TODO: doesn't support RHEL-based certificate store location
        #sock = ssl.wrap_socket(sock, ssl_version=ssl.PROTOCOL_TLSv1)
        # #, cert_reqs=ssl.CERT_REQUIRED, ca_certs="/etc/ssl/certs/ca-certificates.crt")
        #sock.do_handshake()
        # TODO: SSL is broken with asyncore in python 2.7.3 on ubuntu.
        asynchat.async_chat.__init__(self, sock=sock)
        self.ibuffer = []
        self.obuffer = collections.deque()
        self.set_terminator("\r\n")
        self.bot = None

    def register_bot(self, bot):
        self.bot = bot

    def collect_incoming_data(self, data):
        self.ibuffer.append(data)

    def found_terminator(self):
        line = "".join(self.ibuffer)
        self.ibuffer = []
        self.parse(line)

    def parse(self, line):
        temp = line.split(' :')
        prefix, command, params, rest, nick, user, host = [None for i in range(7)]
        if len(temp) == 1:
            if temp[0][0] == ':':
                temp2 = temp[0][1:].split(' ')
                prefix = temp2[0]
                command = temp2[1]
                params = temp2[2:]
            else:
                temp2 = temp[0].split(' ')
                command = temp2[0]
                params = temp2[1:]
        elif len(temp) >= 2:
            if temp[0][0] == ':':
                temp2 = temp[0][1:].split(' ')
                prefix = temp2[0]
                command = temp2[1]
                params = temp2[2:]
            else:
                temp2 = temp[0].split(' ')
                command = temp2[0]
                params = temp2[1:]
            rest = ' :'.join(temp[1:])
            params.append(rest)
        if prefix:
            if '!' in prefix:
                nick, user = prefix.split('!')
            if user and '@' in user:
                user, host = user.split('@')
            elif '@' in prefix:
                nick, host = prefix.split('@')
        self.process(line, prefix, nick, user, host, command, params, rest)


    def process(self, line, prefix, nick, user, host, command, params, rest):
        # If we get a Closing Link message, just quit.
        if line.startswith("ERROR :Closing Link:"):
            sys.exit(0) # We're done here.

        # If the server pings us, respond with a pong.
        if command == 'PING':
            print('Received PING from {}, answering PONG :{}'.format(rest, rest))
            self.push("PONG :{}\n".format(rest))
            return  # early, we're done processing a required server message.

        # If alta versions us, it's time to join a channel. (HACK for OSPNET)
        if user == 'alta':
            self.push("JOIN {}\n".format(botconfig.CHANNEL))
            return  # early, Bot doesn't need to process.

        # If we get other notices from the server, just discard them.
        if host is None:
            return  # early, Bot doesn't need to process.

        # Send the rest over to the Bot class to process.
        self.bot.process(line, prefix, nick, user, host, command, params, rest)


