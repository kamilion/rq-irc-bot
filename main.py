#!/usr/bin/env python
import IRC, stdio, Bot
import asyncore

irc = IRC.IRC()
console = stdio.stdio(irc)
bot = Bot.Bot(irc, console)
asyncore.loop(use_poll=True)
