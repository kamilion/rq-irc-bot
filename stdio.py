import asyncore, asynchat, fcntl, os


class stdio:
    class Input(asynchat.async_chat):
        def __init__(self, parent):
            self.parent = parent
            asynchat.async_chat.__init__(self)
            self.connected = True
            self.socket = asyncore.file_wrapper(0)
            self._fileno = self.socket.fileno()
            self.add_channel()
            flags = fcntl.fcntl(self._fileno, fcntl.F_GETFL, 0)
            flags |= os.O_NONBLOCK
            fcntl.fcntl(self._fileno, fcntl.F_SETFL, flags)
            self.ibuffer = []
            self.set_terminator("\n")

        def writable(self):
            return False

        def collect_incoming_data(self, data):
            self.ibuffer.append(data)

        def found_terminator(self):
            line = "".join(self.ibuffer)
            self.ibuffer = []
            self.parse(line)

        def parse(self, line):
            pass

    class Output(asynchat.async_chat):
        def __init__(self, parent):
            self.parent = parent
            asynchat.async_chat.__init__(self)
            self.connected = True
            self.socket = asyncore.file_wrapper(1)
            self._fileno = self.socket.fileno()
            self.add_channel()
            flags = fcntl.fcntl(self._fileno, fcntl.F_GETFL, 0)
            flags |= os.O_NONBLOCK
            fcntl.fcntl(self._fileno, fcntl.F_SETFL, flags)

        def readable(self):
            return False

    def __init__(self, irc):
        self.irc = irc
        self.input = self.Input(self)
        self.output = self.Output(self)

    def write(self, text):
        self.output.push(text + "\n")
